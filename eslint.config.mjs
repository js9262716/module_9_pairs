import globals from 'globals'
import pluginJs from '@eslint/js'
import eslintConfigPrettier from 'eslint-config-prettier'
import eslintPluginCypress from 'eslint-plugin-cypress'

export default [
	{ languageOptions: { globals: globals.browser } },
	eslintConfigPrettier,
	eslintPluginCypress,
	pluginJs.configs.recommended,
	{
		rules: {
			'no-unused-vars': 'error',
			'no-undef': 'error',
			"cypress/no-assigning-return-values": "error",
			"cypress/no-unnecessary-waiting": "error",
			"cypress/assertion-before-screenshot": "warn",
			"cypress/no-force": "warn",
			"cypress/no-async-tests": "error",
			"cypress/no-async-before": "error",
			"cypress/no-pause": "error"
		},
	},
]
